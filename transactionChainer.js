"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionChainer = void 0;
const mongoose_1 = require("mongoose");
class TransactionChainer {
    constructor(chainedTransactions) {
        this.chainedTransactions = chainedTransactions;
        this.isFakeSession = false;
        /**
         * Determine whether or not to start an active session and starts session if required
         * Checks if process.env.RUNNING_REPLICA is set to "true"
         */
        this.startSessionMethod = () => __awaiter(this, void 0, void 0, function* () {
            let session = process.env.RUNNING_REPLICA === "true" ?
                yield mongoose_1.startSession() :
                (() => {
                    this.isFakeSession = true;
                    return null;
                })();
            return session;
        });
        /**
         * Runs each query using the same transaction context
         */
        this.transactionsRunner = () => __awaiter(this, void 0, void 0, function* () {
            const activeSession = yield this.startSessionMethod();
            if (activeSession) {
                activeSession.startTransaction();
            }
            else {
                return false;
            }
            console.time("Transaction start");
            let success = true;
            for (let data of this.chainedTransactions) {
                let result = yield data.session(activeSession).exec().then((data) => {
                    console.log(data);
                    return true;
                }).catch((e) => {
                    console.log(e.message);
                    /**
                     * For some weird reason, successful transaction commits generate
                     * error messages while committing. The error messages can be ignored
                     * as no negative effect has been found yet. Error code is *256*
                     */
                    if (e && e.code !== 256) {
                        console.log("Error is ", e);
                        activeSession.endSession();
                        return false;
                    }
                    else if (e.code === 256) {
                        success = true;
                    }
                });
                if (!result) {
                    success = false;
                    break;
                }
            }
            if (activeSession.inTransaction()) {
                yield activeSession.commitTransaction();
            }
            activeSession.endSession();
            return success;
        });
    }
}
exports.TransactionChainer = TransactionChainer;
//# sourceMappingURL=transactionChainer.js.map