/**
 * This handles stringified data and filters out
 * unneccessary fields
 * @param {Object} shell
 */
export declare const dataCleaner: (obj: any) => {
    data: any;
    or: any;
    notEqual: any;
    limit: number;
    page: number;
    order: any;
    fields: any;
    range: any;
    find: any;
    search: any;
    pathOfModelToPopulate: string;
    validator: (x: any[]) => boolean;
};
/**
 * This function takes a string containing properties of an object
 * as well as the object and removes those properties from the
 * object
 * @param {Object} obj
 * @param {String} toRemove
 */
export declare const filterObject: (obj: any, toRemove: any) => any;
export declare const tokenVerifier: (req: any, res: any, next: any, secret: any) => any;
