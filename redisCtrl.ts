// Created by @balocodes
import * as redis from "redis";

class RedisCtrl {
  activeClient = this.makeClient();
  constructor() {
  }

  makeClient() {
    return redis.createClient();
  }

  watchConnection() {
    this.activeClient.on("connect", function() {
      console.log("Redis client connected");
    });
    this.activeClient.on('error', function (err) {
        console.log('Something went wrong ' + err);
    });
  }

  set(data: any) {
      this.activeClient.set(data.key, data.value, redis.print);
  }

  get(data: any) {
      this.activeClient.get(data.key, data.func);
  }
}

export const redisCtrl = new RedisCtrl();
