import { Schema, model } from "mongoose";

const logSchema = new Schema({
    error: String
}, {
  timestamps: {createdAt: "created_at", updatedAt: "updated_at"}
});

export const errorLogModel = model("ErrorLog", logSchema);
