"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorLogModel = void 0;
const mongoose_1 = require("mongoose");
const logSchema = new mongoose_1.Schema({
    error: String
}, {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" }
});
exports.errorLogModel = mongoose_1.model("ErrorLog", logSchema);
//# sourceMappingURL=errorLog.js.map