"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logModel = void 0;
const mongoose_1 = require("mongoose");
const logSchema = new mongoose_1.Schema({
    who: {
        type: String,
        required: true
    },
    staff: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Staff"
    },
    action: {
        type: String
    },
    model_name: {
        type: String
    },
    previous_data: {
        type: mongoose_1.Schema.Types.Mixed // If data was edited
    },
    created_at: {
        type: Date,
        once: true,
        default: Date.now
    }
});
exports.logModel = mongoose_1.model("Log", logSchema);
//# sourceMappingURL=log.js.map