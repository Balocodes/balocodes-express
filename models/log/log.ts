import { Schema, model } from "mongoose";

const logSchema = new Schema({
  who: {
    type: String,
    required: true
  },
  staff: {
    type: Schema.Types.ObjectId,
    ref: "Staff"
  },
  action: {
    type: String
  },
  model_name: {
    type: String
  },
  previous_data: {
    type: Schema.Types.Mixed // If data was edited
  },
  created_at: {
    type: Date,
    once: true,
    default: Date.now
  }
});

export const logModel = model("Log", logSchema);
