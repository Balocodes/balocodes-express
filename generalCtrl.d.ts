import { Model, Document } from "mongoose";
import { GeneralUtility } from "./genUtitlity";
interface Population {
    run_population?: boolean;
    population_path?: string;
    population_fields?: string;
}
declare class GeneralCtrl extends GeneralUtility {
    model: Model<Document>;
    response: any;
    request: any;
    sendResp: any;
    /**
     * Expects a db model. Attempts to send a response after operation
     * except sendResp is set to false
     * @param model
     * @param sendResp
     */
    constructor(model: Model<Document>, sendResp?: boolean);
    shellGenerator(req: any): {
        data: any;
        or: any;
        notEqual: any;
        limit: number;
        page: number;
        order: any;
        fields: any;
        range: any;
        find: any;
        search: any;
        pathOfModelToPopulate: string;
        validator: (x: any[]) => boolean;
    };
    shellGenerator2(req: any): {
        data: any;
        or: any;
        notEqual: any;
        limit: number;
        page: number;
        order: any;
        fields: any;
        range: any;
        find: any;
        search: any;
        pathOfModelToPopulate: string;
        validator: (x: any[]) => boolean;
    };
    baloPatcher: (req: any, shell: any) => any;
    fullError(err: any): any;
    /**
     * Adds data to the db
     * ***POST***
     * @param req
     * @param res
     */
    addData(req: any, res: any, callback?: (hasError: boolean) => void): Promise<Document>;
    /**
     * Adds data to the db
     * ***POST***
     * @param req
     * @param res
     * @param options
     */
    addDataV2(req: any, res: any, options?: {
        successMessage: string;
        errorMessage: string;
    }): Promise<{
        result: Document;
        message: string;
        error: boolean;
    } | {
        message: string;
        error: boolean;
        fullError: any;
    }>;
    /**
     * Add Many Items at once. Expects an array in the
     * form of req.body.data
     * @param req Request
     * @param res Response
     */
    addMany(req: any, res: any): void;
    addManyV2(req: any, res: any, options?: {
        successMessage: string;
        errorMessage: string;
    }): Promise<{
        result: Document;
        message: string;
        error: boolean;
    } | {
        message: string;
        error: boolean;
        fullError: any;
    }>;
    addManyWithoutRequest(data: any[]): Promise<{
        result: Document[];
        message: string;
        error: boolean;
    } | {
        message: string;
        error: boolean;
        result?: undefined;
    } | {
        message: string;
        error: boolean;
        fullError: any;
    }>;
    /**
     * Fetch data from db
     * ***GET***
     * @param req
     * @param res
     */
    readData(req: any, res: any, callbackRunner?: {
        population: boolean;
        population_config: {
            pathOfModelToPopulate: string;
            fields: string;
        };
    }): void;
    readDataV2(req: any, res?: any, options?: {
        successMessage: string;
        errorMessage: string;
    }, population?: Population): Promise<{
        result: Document[];
        message: string;
        error: boolean;
    } | {
        message: string;
        error: boolean;
        fullError: any;
    }>;
    /**
     * Fetch data by ID
     * ***GET***
     * @param req
     * @param res
     */
    readDataById(req: any, res: any, callbackRunner?: {
        population: boolean;
        population_config: {
            pathOfModelToPopulate: string;
            fields: string;
        };
    }): Promise<void> | undefined;
    readDataByIdV2(req: any, res?: any, options?: {
        successMessage: string;
        errorMessage: string;
    }, population?: Population): Promise<{
        result: Document[];
        message: string;
        error: boolean;
    } | {
        message: string;
        error: boolean;
        fullError: any;
    }>;
    /**
     * Fetch single item from db
     * ***GET***
     * @param req
     * @param res
     */
    readSingleItem({ req, res, callbackRunner, }: {
        req: any;
        res: any;
        callbackRunner?: {
            population: boolean;
            population_config: {
                pathOfModelToPopulate: string;
                fields: string;
            };
        };
    }): void;
    readSingleItemV2({ req, _res, options, population, }: {
        req: any;
        _res?: any;
        options?: {
            successMessage: string;
            errorMessage: string;
        };
        population?: Population;
    }): Promise<{
        result: Document | null;
        message: string;
        error: boolean;
    }>;
    /**
     * Find by id and update
     * ***PUT***
     * @param req
     * @param res
     */
    updateDataById(req: any, res: any, callback?: (hasError: boolean) => void): Promise<void> | undefined;
    updateDataByIdV2(req: any, res: any, options?: {
        successMessage: string;
        errorMessage: string;
    }): Promise<{
        result: Document | null;
        message: string;
        error: boolean;
    } | {
        message: string;
        error: boolean;
        fullError: any;
    }>;
    /**
     * Find and update. Requires a find field in the request body
     * ***PUT***
     * @param req
     * @param res
     */
    updateData(req: any, res: any, callback?: (hasError: boolean) => void): void;
    /**
     * Find document by id and delete
     * ***DELETE***
     * @param req
     * @param res
     */
    deleteDataById(req: any, res: any, callback?: (hasError: boolean) => void): Promise<void> | undefined;
    deleteDataByIdV2(req: any, res: any, options?: {
        successMessage: string;
        errorMessage: string;
    }): Promise<{
        result: Document | null;
        message: string;
        error: boolean;
    } | {
        message: string;
        error: boolean;
        fullError: any;
    }>;
    searchDataV2({ req, _res, options, population, }: {
        req: any;
        _res?: any;
        options?: {
            successMessage: string;
            errorMessage: string;
        };
        population?: Population;
    }): Promise<{
        result: Document[];
        message: null;
        error: boolean;
    } | {
        result: null;
        message: string;
        error: boolean;
    }>;
    /**
     * Search for data in db. Requires a search field.
     * ***GET***
     * @param req
     * @param res
     */
    searchData(req: any, res: any): void;
    /**
     * Populate nested models in result by id
     * @param results
     * @param obj
     */
    populateModel(results: any, obj: any): void;
    /**
     * Returns the estimated count of documents in a collection using
     * the meta data. May not be as accurate as a full count.
     * **GET**
     * @param req
     * @param res
     */
    estimatedCount(req: any, res: any): void;
    estimatedCountV2(req: any, res: any): Promise<{
        count: number;
        error: boolean;
        message: string;
    } | {
        error: boolean;
        message: string;
        fullError: any;
    }>;
    /**
     * Increase or decrease value of a field
     * @param data e.g. { find: {_id: "57485"}, inc: {age: 2} }
     */
    incrementor(model: Model<Document>, data: {
        find: {
            [params: string]: any;
        };
        inc: {
            [params: string]: any;
        };
    }): Promise<any>;
    /**
     * Returns the estimated count of documents in a collection using
     * the meta data. May not be as accurate as a full count.
     * **GET**
     * @param req
     * @param res
     */
    countData(req: any, res: any): void;
    countDataV2(req: any, res: any): Promise<{
        count: number;
        error: boolean;
        message: string;
    } | {
        error: boolean;
        message: string;
        fullError: any;
    }>;
    /**
     * Returns false if data exists and true if it does not
     * Expects a query object to search
     * @param query
     */
    isDataUnique(query: any): Promise<boolean>;
}
export declare const GenCtrl: typeof GeneralCtrl;
export {};
