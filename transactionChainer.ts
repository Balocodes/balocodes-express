import { ClientSession, startSession, DocumentQuery, Document } from "mongoose";

export class TransactionChainer {
    isFakeSession = false

    constructor(
        public chainedTransactions: DocumentQuery<Document | null, Document, {}>[],
    ) {    }

    /**
     * Determine whether or not to start an active session and starts session if required
     * Checks if process.env.RUNNING_REPLICA is set to "true"
     */
    startSessionMethod = async (): Promise<ClientSession | null> => {
        let session: ClientSession | null = process.env.RUNNING_REPLICA === "true" ?
            await startSession() :
            (() => {
                this.isFakeSession = true
                return null
            })();
        return session
    }

    /**
     * Runs each query using the same transaction context
     */
    transactionsRunner = async () => {
        const activeSession = await this.startSessionMethod()
        if(activeSession) {
            activeSession.startTransaction()
        } else {
            return false
        }
        
        console.time("Transaction start")
        let success = true
        for (let data of this.chainedTransactions) {
            let result = await data.session(activeSession).exec().then((data) => {
                console.log(data)
                return true
            }).catch((e) => {
                console.log(e.message);
                /** 
                 * For some weird reason, successful transaction commits generate
                 * error messages while committing. The error messages can be ignored
                 * as no negative effect has been found yet. Error code is *256*
                 */
                if (e && e.code !== 256) {
                    console.log("Error is ", e)
                    activeSession.endSession()
                    return false
                } else if(e.code === 256) {
                    success = true
                } 
            })
            if(!result) {
                success = false
                break
            }
        }
        if(activeSession.inTransaction()) {
            await activeSession.commitTransaction()
        }
        activeSession.endSession()
        return success
    }

}