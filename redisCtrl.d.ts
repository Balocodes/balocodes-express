import * as redis from "redis";
declare class RedisCtrl {
    activeClient: redis.RedisClient;
    constructor();
    makeClient(): redis.RedisClient;
    watchConnection(): void;
    set(data: any): void;
    get(data: any): void;
}
export declare const redisCtrl: RedisCtrl;
export {};
