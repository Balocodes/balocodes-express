import { ModelPopulateOptions, Model, Document } from "mongoose";
import { GenCtrl } from "../generalCtrl";
import { Socket } from "socket.io";
import { Request, Response } from "express";
interface RouterConfig {
  autoRespond?: boolean;
  /**
   *  e.g [{"id": "_id"}, {"date": "created_at"}]
   */
  params?: { [param: string]: string }[];
  ioParams?: {
    IOEvent: string;
    IOContent?: any;
  };
  /**
   * path: string;
   * select: string;
   * match: optional query conditions to match;
   * model: optional name of model to use for poulation;
   * options: optional query parameters like sort, limit, etc;
   * populate: ModelPopulateOptions | ModelPopulateOptions;
   */
  populationOptions?: ModelPopulateOptions;
  baloPatch?: { [param: string]: any };
  request:
    | "read-all"
    | "read-single"
    | "search"
    | "add-data"
    | "update-data-by-id"
    | "delete-data";
}

/**
 * Indicates how params should be interpreted and used in GenCtrl
 * e.g. params = [{request_id: "_id", name: "fname"}]
 * will return a request with req.params.baloPatch = {"_id": "1234", "fname": "Amin"}
 * @param params
 * @param req
 */
async function paramsHandler(params: RouterConfig["params"], req: Request) {
  let newParams = {};
  if (params) {
    params.forEach(element => {
      let key = Object.keys(element)[0];
      let val = element[key];
      newParams = { ...newParams, ...{ [val]: req.params[key] } };
    });
  }
  if(!req.params.baloPatch) {
        req.params.baloPatch = newParams
    } else {
        req.params.baloPatch = {...req.params.baloPatch, ...newParams}
    }
  return req;
}

async function ioParamsHandler(
  ioParams: RouterConfig["ioParams"],
  req: Request
) {
  if (ioParams) {
    req.params.IOEvent = ioParams.IOEvent;
    req.params.IOContent = ioParams.IOContent;
  }
  return req;
}

async function baloPatchHandler(
  patch: RouterConfig["baloPatch"],
  req: Request
) {
  req.params.baloPatch = { ...req.params.baloPatch, ...patch };
  return req;
}

/**
 * Prepares the population data for GenCtrl
 * @param populationOptions
 * @param req
 */
async function prepForPopulation(
  populationOptions: ModelPopulateOptions,
  req: Request
) {
  req.params.run_population = true;
  req.params.population_path = populationOptions.path;
  req.params.population_fields = populationOptions.select;
  return req;
}

async function requestFormatter(req: Request, config: RouterConfig) {
  // Patch params correctly and reassign
  req = await paramsHandler(config.params, req);

  // Use Extra BaloPatch if available
  req = await baloPatchHandler(config.baloPatch, req);

  // Patch IO event
  req = await ioParamsHandler(config.ioParams, req);

  return req;
}

/**
 * A Decorator created give CRUD capabilities to decorated methods
 * @param model the database model
 * @param config configuration to indicate which request to use and how
 */
export function consume(
  model: Model<Document>,
  config: RouterConfig = { request: "read-all" }
) {
  return function(
    target: Object,
    propertyName: string,
    propertyDescriptor: PropertyDescriptor
  ) {
    const method = propertyDescriptor.value;
    propertyDescriptor.value = async function(
      ...args: [Request, Response, any]
    ) {
      let result; // Result of database operation
      let instance = new GenCtrl(model); // Instance of GenCtrl

      // Format Request by adding necessary data in correct format
      args[0] = await requestFormatter(args[0], config);

      if (config.populationOptions) {
        args[0] = await prepForPopulation(config.populationOptions, args[0]);
      }
      switch (config.request) {
        case "read-all":
          result = await instance.readDataByIdV2(args[0]);
          break;
        case "read-single":
          result = await instance.readSingleItemV2({ req: args[0] });
          break;
        case "search":
          result = await instance.searchDataV2({ req: args[0], _res: args[1] });
          break;
        case "add-data":
          result = await instance.addDataV2(args[0], args[1]);
          break;
        case "update-data-by-id":
          result = await instance.updateDataByIdV2(args[0], args[1]);
          break;
        case "delete-data":
          result = await instance.deleteDataByIdV2(args[0], args[1]);
          break;
        default:
          result = { message: "No Request type indicated", error: true };
          break;
      }
      args[0].params.finalResult = result;
      config.autoRespond !== false ? args[1].send(result) : (() => {})();
      return method.apply(this, args);
    };
    return propertyDescriptor;
  };
}
