import { ModelPopulateOptions, Model, Document } from "mongoose";
interface RouterConfig {
    autoRespond?: boolean;
    /**
     *  e.g [{"id": "_id"}, {"date": "created_at"}]
     */
    params?: {
        [param: string]: string;
    }[];
    ioParams?: {
        IOEvent: string;
        IOContent?: any;
    };
    /**
     * path: string;
     * select: string;
     * match: optional query conditions to match;
     * model: optional name of model to use for poulation;
     * options: optional query parameters like sort, limit, etc;
     * populate: ModelPopulateOptions | ModelPopulateOptions;
     */
    populationOptions?: ModelPopulateOptions;
    baloPatch?: {
        [param: string]: any;
    };
    request: "read-all" | "read-single" | "search" | "add-data" | "update-data-by-id" | "delete-data";
}
/**
 * A Decorator created give CRUD capabilities to decorated methods
 * @param model the database model
 * @param config configuration to indicate which request to use and how
 */
export declare function consume(model: Model<Document>, config?: RouterConfig): (target: Object, propertyName: string, propertyDescriptor: PropertyDescriptor) => PropertyDescriptor;
export {};
