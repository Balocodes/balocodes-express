"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenCtrl = void 0;
const genUtitlity_1 = require("./genUtitlity");
class GeneralCtrl extends genUtitlity_1.GeneralUtility {
    /**
     * Expects a db model. Attempts to send a response after operation
     * except sendResp is set to false
     * @param model
     * @param sendResp
     */
    constructor(model, sendResp = true) {
        super(model);
        // Patches extra fields onto shell.data when required
        this.baloPatcher = (req, shell) => {
            if (req.baloPatch) {
                return Object.assign(Object.assign({}, shell.data), req.baloPatch);
            }
            else {
                return shell.data;
            }
        };
        this.model = model;
        this.sendResp = sendResp;
        // console.log(process.env.NODE_ENV);
    }
    // Generates shell from req.query
    shellGenerator(req) {
        let shell = this.dataCleaner(req.query);
        shell.data = this.baloPatcher(req, shell);
        if (req.params.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.params.baloPatch);
        }
        return this.dataCleaner(shell);
    }
    // Generates shell from req.body
    shellGenerator2(req) {
        let shell = this.dataCleaner(req.body);
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        if (req.params.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.params.baloPatch);
        }
        return this.dataCleaner(shell);
    }
    // Returns full err when in development mode. Used when returning response to user
    fullError(err) {
        if (process.env.NODE_ENV === "development") {
            return err;
        }
        else {
            return {};
        }
    }
    /**
     * Adds data to the db
     * ***POST***
     * @param req
     * @param res
     */
    addData(req, res, callback = this.afterRequestCallback) {
        this.response = res;
        this.request = req;
        const shell = this.dataCleaner(req.body);
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        return new this.model(shell.data).save((err, data) => {
            if (this.request.logger) {
                GeneralCtrl.loggerV2({
                    who: req.decoded.username,
                    staff: req.decoded._id,
                    action: `Attempted to add data: ${data ? data._id : "Seemed to fail"}`,
                    modelName: this.model.modelName,
                });
            }
            this.defaultCallback(err, data, this.respCode(err, 201, callback));
        });
    }
    /**
     * Adds data to the db
     * ***POST***
     * @param req
     * @param res
     * @param options
     */
    addDataV2(req, res, options = {
        successMessage: "Data added successfully!",
        errorMessage: "Failed to add data",
    }) {
        return __awaiter(this, void 0, void 0, function* () {
            const shell = this.shellGenerator2(req);
            return yield new this.model(shell.data)
                .save()
                .then((data) => {
                if (req.logger) {
                    GeneralCtrl.logger(req.decoded._id, `Attempted to add data: ${data._id}`, this.model.modelName);
                }
                // this.successHandler();
                genUtitlity_1.GeneralUtility.successHandler({
                    io: req.IO,
                    event: req.params.IOEvent,
                    content: req.params.IOContent,
                });
                return { result: data, message: options.successMessage, error: false };
            })
                .catch((err) => {
                GeneralCtrl.printData("Error while adding data", err);
                if (req.logger) {
                    GeneralCtrl.loggerV2({
                        who: req.decoded.username,
                        staff: req.decoded._id,
                        action: `Attempted to add data: "Seemed to fail"}`,
                        modelName: this.model.modelName,
                    });
                }
                this.errorHandler();
                return {
                    message: options.errorMessage,
                    error: true,
                    fullError: this.fullError(err),
                };
            });
        });
    }
    /**
     * Add Many Items at once. Expects an array in the
     * form of req.body.data
     * @param req Request
     * @param res Response
     */
    addMany(req, res) {
        this.response = res;
        this.request = req;
        this.model.insertMany(req.body.data, {
            ordered: false,
        }, (err, data) => {
            if (this.request.logger) {
                GeneralCtrl.logger(this.request.decoded._id, "Attempted to add multiple data", this.model.modelName);
            }
            if (err) {
                GeneralCtrl.printData(err);
                this.response.send({
                    message: "An error occured. Ignoring duplicate data",
                    error: true,
                });
                if (this.request.logger) {
                    GeneralCtrl.logger(this.request.decoded._id, "Failed: Add many failed", this.model.modelName);
                }
            }
            else {
                if (this.request.logger) {
                    GeneralCtrl.logger(this.request.decoded._id, "Added multiple data successfully!", this.model.modelName);
                }
                this.response.send({
                    message: `Data successfully added ${data.length} entries`,
                    error: false,
                });
            }
        });
    }
    addManyV2(req, res, options = {
        successMessage: "Data added successfully!",
        errorMessage: "Failed to add data",
    }) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.model
                .insertMany(req.body.data, { ordered: false })
                .then((data) => {
                if (req.logger) {
                    GeneralCtrl.logger(req.decoded._id, `Attempted to add multiple data: "Successfully added data"}`, this.model.modelName);
                }
                this.successHandler();
                return { result: data, message: options.successMessage, error: false };
            })
                .catch((err) => {
                if (req.logger) {
                    GeneralCtrl.loggerV2({
                        who: req.decoded.username,
                        staff: req.decoded._id,
                        action: `Attempted to add multiple data:  "Seemed to fail"}`,
                        modelName: this.model.modelName,
                    });
                }
                this.errorHandler();
                return {
                    message: options.errorMessage,
                    error: true,
                    fullError: this.fullError(err),
                };
            });
        });
    }
    addManyWithoutRequest(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.model
                .insertMany(data, { ordered: false })
                .then((data) => {
                if (data && data.length > 0) {
                    return {
                        result: data,
                        message: "Operation Successful",
                        error: false,
                    };
                }
                else {
                    return { message: "No data was added", error: false };
                }
            })
                .catch((e) => {
                return { message: "An error occured", error: true, fullError: e };
            });
        });
    }
    /**
     * Fetch data from db
     * ***GET***
     * @param req
     * @param res
     */
    readData(req, res, callbackRunner = {
        population: false,
        population_config: {
            pathOfModelToPopulate: "",
            fields: "",
        },
    }) {
        this.response = res;
        this.request = req;
        let shell = this.dataCleaner(req.query);
        // Balo patch extends the content of the data after it has been cleaned
        // Makes it easier to patch extra parameters to query.
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        this.model
            .find(shell.data)
            .limit(shell.limit)
            .skip(shell.page * shell.limit)
            .sort(shell.order)
            .select(shell.fields)
            .exec((err, data) => {
            if (callbackRunner.population) {
                this.populateModel(data, callbackRunner.population_config);
            }
            else {
                this.defaultCallback(err, data, this.respCode(err, 200));
            }
        });
    }
    readDataV2(req, res, options = {
        successMessage: "Operation Successful!",
        errorMessage: "Failed to read data",
    }, population = { run_population: false }) {
        return __awaiter(this, void 0, void 0, function* () {
            const shell = this.shellGenerator(req);
            if (req.params.run_population) {
                population.run_population = true;
                population.population_fields = req.params.population_fields;
                population.population_path = req.params.population_path;
            }
            return population.run_population
                ? yield this.model
                    .find(shell.data)
                    .limit(shell.limit)
                    .skip(shell.limit * shell.page)
                    .sort(shell.order)
                    .select(shell.fields)
                    .populate(population.population_path, population.population_fields)
                    .then((data) => {
                    return {
                        result: data,
                        message: options.successMessage,
                        error: false,
                    };
                })
                    .catch((err) => {
                    console.log("err 1", err);
                    return {
                        message: options.errorMessage,
                        error: true,
                        fullError: this.fullError(err),
                    };
                })
                : yield this.model
                    .find(shell.data)
                    .limit(shell.limit)
                    .skip(shell.limit * shell.page)
                    .sort(shell.order)
                    .then((data) => {
                    return {
                        result: data,
                        message: options.successMessage,
                        error: false,
                    };
                })
                    .catch((err) => {
                    console.log(err);
                    return {
                        message: options.errorMessage,
                        error: true,
                        fullError: this.fullError(err),
                    };
                });
        });
    }
    /**
     * Fetch data by ID
     * ***GET***
     * @param req
     * @param res
     */
    readDataById(req, res, callbackRunner = {
        population: false,
        population_config: {
            pathOfModelToPopulate: "",
            fields: "",
        },
    }) {
        this.response = res;
        this.request = req;
        if (!req.data.query._id) {
            return this.defaultCallback({ code: "NO_ID", message: "No ID present on data" }, null);
        }
        this.model.findById(req.data.query._id).exec((err, data) => {
            if (callbackRunner.population) {
                this.populateModel(data, callbackRunner.population_config);
            }
            else {
                this.defaultCallback(err, data, this.respCode(err, 200));
            }
        });
    }
    readDataByIdV2(req, res, options = {
        successMessage: "Operation Successful!",
        errorMessage: "Failed to read data",
    }, population = { run_population: false }) {
        return __awaiter(this, void 0, void 0, function* () {
            const shell = this.shellGenerator(req);
            if (req.params.run_population) {
                population.run_population = true;
                population.population_fields = req.params.population_fields;
                population.population_path = req.params.population_path;
            }
            return population.run_population
                ? yield this.model
                    .find(shell.data)
                    .limit(shell.limit)
                    .skip(shell.limit * shell.page)
                    .sort(shell.order)
                    .select(shell.fields)
                    .populate(population.population_path, population.population_fields)
                    .then((data) => {
                    return {
                        result: data,
                        message: options.successMessage,
                        error: false,
                    };
                })
                    .catch((err) => {
                    return {
                        message: options.errorMessage,
                        error: true,
                        fullError: this.fullError(err),
                    };
                })
                : yield this.model
                    .find(shell.data)
                    .limit(shell.limit)
                    .skip(shell.limit * shell.page)
                    .select(shell.fields)
                    .sort(shell.order)
                    .then((data) => {
                    return {
                        result: data,
                        message: options.successMessage,
                        error: false,
                    };
                })
                    .catch((err) => {
                    return {
                        message: options.errorMessage,
                        error: true,
                        fullError: this.fullError(err),
                    };
                });
        });
    }
    /**
     * Fetch single item from db
     * ***GET***
     * @param req
     * @param res
     */
    readSingleItem({ req, res, callbackRunner = {
        population: false,
        population_config: {
            pathOfModelToPopulate: "",
            fields: "",
        },
    }, }) {
        this.response = res;
        this.request = req;
        const shell = this.dataCleaner(req.query);
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        this.model
            .findOne(shell.data)
            .select(shell.fields)
            .exec((err, data) => {
            if (callbackRunner.population) {
                this.populateModel(data, callbackRunner.population_config);
            }
            else {
                this.defaultCallback(err, data, this.respCode(err, 200));
            }
        });
    }
    readSingleItemV2({ req, _res, options = {
        successMessage: "Operation Successful!",
        errorMessage: "Operation failed!",
    }, population = { run_population: false }, }) {
        return __awaiter(this, void 0, void 0, function* () {
            const shell = this.shellGenerator(req);
            if (req.params.run_population) {
                population.run_population = true;
                population.population_fields = req.params.population_fields;
                population.population_path = req.params.population_path;
            }
            return population.run_population
                ? yield this.model
                    .findOne(shell.data)
                    .skip(shell.limit * shell.page)
                    .sort(shell.order)
                    .select(shell.fields)
                    .populate(population.population_path, population.population_fields)
                    .then((data) => {
                    return {
                        result: data,
                        message: options.successMessage,
                        error: false,
                    };
                })
                    .catch((err) => {
                    return {
                        result: null,
                        message: options.errorMessage,
                        error: true,
                        fullError: this.fullError(err),
                    };
                })
                : yield this.model
                    .findOne(shell.data)
                    .skip(shell.limit * shell.page)
                    .select(shell.fields)
                    .sort(shell.order)
                    .then((data) => {
                    return {
                        result: data,
                        message: data ? options.successMessage : "No document found.",
                        error: false,
                    };
                })
                    .catch((err) => {
                    return {
                        result: null,
                        message: options.errorMessage,
                        error: true,
                        fullError: this.fullError(err),
                    };
                });
        });
    }
    /**
     * Find by id and update
     * ***PUT***
     * @param req
     * @param res
     */
    updateDataById(req, res, callback = this.afterRequestCallback) {
        this.response = res;
        this.request = req;
        // this.sendResp = true;
        const shell = this.dataCleaner(req.body);
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        if (!shell.data._id) {
            return this.defaultCallback({ code: "NO_ID", message: "No ID present on data" }, null);
        }
        this.model
            .findByIdAndUpdate(shell.data._id, { $set: shell.data })
            .exec((err, data) => {
            if (this.request.logger) {
                GeneralCtrl.logger(this.request.decoded._id, `Attempted to update data: ${shell.data._id}`, this.model.modelName, data);
            }
            this.defaultCallback(err, data, this.respCode(err, 200, callback));
        });
    }
    updateDataByIdV2(req, res, options = {
        successMessage: "Data updated successfully!",
        errorMessage: "Failed to update data",
    }) {
        return __awaiter(this, void 0, void 0, function* () {
            const shell = this.shellGenerator2(req);
            return yield this.model
                .findByIdAndUpdate(shell.data._id, { $set: shell.data })
                .then((data) => {
                if (req.logger) {
                    GeneralCtrl.logger(req.decoded._id, `Attempted to update data: ${data ? data._id : ""}`, this.model.modelName, data);
                }
                this.successHandler();
                return {
                    result: data,
                    message: data ? options.successMessage : "Document not found.",
                    error: false,
                };
            })
                .catch((err) => {
                if (req.logger) {
                    GeneralCtrl.loggerV2({
                        who: req.decoded.username,
                        staff: req.decoded._id,
                        action: `Attempted to update data: ${shell.data._id} but "Seemed to fail"}`,
                        modelName: this.model.modelName,
                    });
                }
                this.errorHandler();
                return {
                    message: options.errorMessage,
                    error: true,
                    fullError: this.fullError(err),
                };
            });
        });
    }
    /**
     * Find and update. Requires a find field in the request body
     * ***PUT***
     * @param req
     * @param res
     */
    updateData(req, res, callback = this.afterRequestCallback) {
        this.response = res;
        this.request = req;
        const shell = this.dataCleaner(req.body);
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        this.model.update(shell.find, shell.data).exec((err, data) => {
            if (this.request.logger) {
                GeneralCtrl.logger(this.request.decoded._id, `Attempted to update data: ${JSON.stringify(shell.data.find)}`, this.model.modelName, data);
            }
            this.defaultCallback(err, data, this.respCode(err, 200, callback));
        });
    }
    /**
     * Find document by id and delete
     * ***DELETE***
     * @param req
     * @param res
     */
    deleteDataById(req, res, callback = this.afterRequestCallback) {
        this.response = res;
        this.request = req;
        const shell = this.dataCleaner(req.body);
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        if (!shell.data._id) {
            return this.defaultCallback({ code: "NO_ID", message: "No ID present on data" }, null);
        }
        this.model.findByIdAndDelete(shell.data._id).exec((err, data) => {
            if (req.logger) {
                GeneralCtrl.logger(this.request.decoded._id, `Attempted to delete data: ${shell.data._id}`, this.model.modelName, data);
            }
            this.defaultCallback(err, data, this.respCode(err, 200, callback));
        });
    }
    deleteDataByIdV2(req, res, options = {
        successMessage: "Data deleted successfully!",
        errorMessage: "Failed to update data",
    }) {
        return __awaiter(this, void 0, void 0, function* () {
            const shell = this.shellGenerator2(req);
            return yield this.model
                .findByIdAndDelete(shell.data._id)
                .then((data) => {
                if (req.logger) {
                    GeneralCtrl.logger(req.decoded._id, data
                        ? `Attempted to delete data: ${shell.data._id}`
                        : "Attempted to delete noexistent data", this.model.modelName, data);
                }
                GeneralCtrl.printData("delete data", data);
                this.successHandler();
                return {
                    result: data,
                    message: data
                        ? options.successMessage
                        : "Did not find document to delete.",
                    error: false,
                };
            })
                .catch((err) => {
                if (req.logger) {
                    GeneralCtrl.logger(req.decoded._id, `Attempted to delete data: ${shell.data._id} but failed!`, this.model.modelName);
                }
                console.log("delete error", err);
                this.errorHandler();
                return {
                    message: options.errorMessage,
                    error: true,
                    fullError: this.fullError(err),
                };
            });
        });
    }
    searchDataV2({ req, _res, options = {
        successMessage: "Operation Successful!",
        errorMessage: "Operation failed!",
    }, population = { run_population: false }, }) {
        return __awaiter(this, void 0, void 0, function* () {
            const shell = this.shellGenerator(req);
            if (req.params.run_population) {
                population.run_population = true;
                population.population_fields = req.params.population_fields;
                population.population_path = req.params.population_path;
            }
            if (population.run_population) {
                return yield this.model
                    .find({ $text: { $search: shell.data.search, $diacraticSensitive: false } }, { score: { $meta: "textScore" } })
                    .populate(population.population_path, population.population_fields)
                    .limit(shell.limit)
                    .skip(shell.page * shell.limit)
                    .select(shell.fields)
                    .sort(shell.order)
                    .then((data) => {
                    return { result: data, message: null, error: false };
                })
                    .catch((err) => {
                    console.log(err);
                    return { result: null, message: "An error occured", error: true };
                });
            }
            else {
                return yield this.model
                    .find({ $text: { $search: shell.data.search, $diacraticSensitive: false } }, { score: { $meta: "textScore" } })
                    .select(shell.fields)
                    .then((data) => {
                    return { result: data, message: null, error: false };
                })
                    .catch((err) => {
                    console.log(err);
                    return { result: null, message: "An error occured", error: true };
                });
            }
        });
    }
    /**
     * Search for data in db. Requires a search field.
     * ***GET***
     * @param req
     * @param res
     */
    searchData(req, res) {
        this.response = res;
        this.request = req;
        const shell = this.dataCleaner(req.query);
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        this.model
            .find({ $text: { $search: shell.data.search, $diacraticSensitive: false } }, { score: { $meta: "textScore" } })
            .limit(shell.limit)
            .skip(shell.page * shell.limit)
            .sort({ score: { $meta: "textScore" } })
            .select(shell.fields)
            .exec((err, data) => {
            this.defaultCallback(err, data, this.respCode(err, 200));
        });
    }
    /**
     * Populate nested models in result by id
     * @param results
     * @param obj
     */
    populateModel(results, obj) {
        this.model.populate(results, {
            path: obj.pathOfModelToPopulate,
            select: obj.fields,
        }, (err, data) => {
            if (err) {
                GeneralCtrl.printData(err);
                if (this.response != null) {
                    this.response.status(500).send({
                        message: "An error occured during population",
                        code: 500,
                        error: true,
                    });
                }
            }
            else if (this.response != null) {
                this.response.status(200).send({
                    result: data,
                    message: "Query Successful",
                    code: 200,
                    error: false,
                });
            }
        });
    }
    /**
     * Returns the estimated count of documents in a collection using
     * the meta data. May not be as accurate as a full count.
     * **GET**
     * @param req
     * @param res
     */
    estimatedCount(req, res) {
        this.request = req;
        this.response = res;
        this.model.estimatedDocumentCount((err, count) => {
            this.defaultCallback(err, { count: count }, this.respCode(err, 200));
        });
    }
    estimatedCountV2(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.model
                .estimatedDocumentCount()
                .then((count) => {
                return { count: count, error: false, message: "Operation Successful" };
            })
                .catch((err) => {
                return {
                    error: true,
                    message: "Failed to get estimated number of items",
                    fullError: this.fullError(err),
                };
            });
        });
    }
    /**
     * Increase or decrease value of a field
     * @param data e.g. { find: {_id: "57485"}, inc: {age: 2} }
     */
    incrementor(model, data) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield model.update(data.find, { $inc: data.inc });
        });
    }
    /**
     * Returns the estimated count of documents in a collection using
     * the meta data. May not be as accurate as a full count.
     * **GET**
     * @param req
     * @param res
     */
    countData(req, res) {
        this.response = res;
        this.request = req;
        const shell = this.dataCleaner(req.body);
        if (req.baloPatch) {
            shell.data = Object.assign(Object.assign({}, shell.data), req.baloPatch);
        }
        this.model.count(shell.data, (err, count) => {
            this.defaultCallback(err, { count: count }, this.respCode(err, 200));
        });
    }
    countDataV2(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const shell = this.shellGenerator(req);
            return yield this.model
                .count(shell.data)
                .then((count) => {
                return { count: count, error: false, message: "Operation Successful" };
            })
                .catch((err) => {
                return {
                    error: true,
                    message: "Failed to count documents",
                    fullError: this.fullError(err),
                };
            });
        });
    }
    /**
     * Returns false if data exists and true if it does not
     * Expects a query object to search
     * @param query
     */
    isDataUnique(query) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.model.findOne(query);
            if (data) {
                return false;
            }
            else {
                return true;
            }
        });
    }
}
exports.GenCtrl = GeneralCtrl;
//# sourceMappingURL=generalCtrl.js.map