// Created by @balocodes
import Mailgun = require("mailgun-js");
const mailgun = Mailgun({
  apiKey: String(process.env.MAILGUN_API_KEY),
  domain: String(process.env.MAILGUN_DOMAIN)
});

/**
 * Mailgun Controller
*/
class MailCtrl {
  content_type: string;
  sender: string;
  receiver: string;
  subject: string;
  content: string;
  response: any;
  /**
   *
   * @param {String} content_type ("text" or "html")
   * @param {String} sender (Email of sender)
   * @param {String} receiver (Email of receiver)
   * @param {String} subject (Subject of email)
   * @param {String} content (The content of the email)
   * @param {Object} response (Express response)
   */
  constructor(
    content_type: string,
    sender: string,
    receiver: string,
    subject: string,
    content: string,
    response = null
  ) {
    this.content_type = content_type;
    this.sender = sender;
    this.receiver = receiver;
    this.subject = subject;
    this.content = content;
    this.response = response;
  }

  /**
   * Triggers the send mail method
   */
  send() {
    let mailData: any = {};
    mailData["from"] = this.sender;
    mailData["to"] = this.receiver;
    mailData["subject"] = this.subject;
    mailData[this.content_type] = this.content;

    mailgun.messages().send(mailData, (err: any, body: any) => {
      if (err) {
        console.log(err);
        if (this.response) {
          return this.response.send({
            message: "An error occured",
            code: 500,
            error: true
          });
        }
      } else {
        console.log(body);
        if (this.response) {
          this.response.send({
            message: `Email has been sent to ${this.receiver}.`,
            code: 200,
            error: false
          });
        }
      }
    });
  }
}

export const MailController = MailCtrl;
