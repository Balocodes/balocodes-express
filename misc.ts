// Created by @balocodes
// import { connect, Mongoose } from "mongoose";
import jwt from "jsonwebtoken";
import { Request, Response } from "express";
const fs = require("fs");
const csv = require("csvtojson");
const formidable = require("formidable");

/**
 * This handles stringified data and filters out
 * unneccessary fields
 * @param {Object} shell
 */
export const dataCleaner = function dataCleaner(obj: any) {
  let shell = {
    data: obj.data || {},
    or: obj.or,
    notEqual: obj.notEqual,
    limit: Number(obj.limit),
    page: Number(obj.page),
    order: obj.order,
    fields: obj.fields,
    range: obj.range,
    find: obj.find,
    search: obj.search,
    pathOfModelToPopulate: "",
    validator: (x: any[]) => {
      let validated = true;
      for (let i in x) {
        if (x[i] === "" || x[i] === null || x[i] === undefined) {
          validated = false;
        }
      }
      return validated;
    }
  };
  if (typeof shell.data === "string") {
    shell.data = JSON.parse(shell.data);
  }
  if (typeof shell.find === "string") {
    shell.find = JSON.parse(shell.find);
  }
  if (shell.or) {
    if (typeof shell.or === "string") {
      shell.or = JSON.parse(shell.or);
    }
    shell.data = { ...shell.data, $or: [...shell.or] };
  }
  if (shell.notEqual) {
    if (typeof shell.notEqual === "string") {
      shell.notEqual = JSON.parse(shell.notEqual);
    }
    shell.notEqual.forEach((element: { [param: string]: any }) => {
      shell.data = {
        ...shell.data,
        ...{ [element.key]: { $ne: element.value } }
      };
    });
  }
  if (!shell.limit) {
    shell.limit = 100;
  }
  if (!shell.page) {
    shell.page = 0;
  }
  if (!shell.order) {
    shell.order = "-_id";
  }
  if (shell.range) {
    if (typeof shell.range === "string") {
      shell.range = JSON.parse(shell.range);
    }
    for (let x = 0; x < shell.range.length; x++) {
      if (shell.range[x].field && shell.range[x].gte && shell.range[x].lte) {
        shell.data[shell.range[x].field] = {
          $gte: shell.range[x].gte,
          $lte: shell.range[x].lte
        };
      } else if (shell.range[x].field && shell.range[x].gte) {
        shell.data[shell.range[x].field] = {
          $gte: shell.range[x].gte
        };
      } else if (shell.range[x].field && shell.range[x].lte) {
        shell.data[shell.range[x].field] = {
          $lte: shell.range[x].lte
        };
      }
    }
  }
  return shell;
};

/**
 * This function takes a string containing properties of an object
 * as well as the object and removes those properties from the
 * object
 * @param {Object} obj
 * @param {String} toRemove
 */
export const filterObject = function filterObject(obj: any, toRemove: any) {
  if (!toRemove) {
    return obj;
  }
  let counter = 0;
  let removeArr = toRemove.split(" ");
  for (let x in obj) {
    if (removeArr.includes(x)) {
      delete obj[x];
    }
  }
  return obj;
};

export const tokenVerifier = (req: any, res: any, next: any, secret: any) => {
  let token = req.body.token || req.query.token;

  try {
    req.decoded = jwt.verify(token, String(secret));
    next();
  } catch (error) {
    return res.send({
      message: "Verification failed. Please log in again.",
      code: 401,
      error: true
    });
  }
};
