"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.redisCtrl = void 0;
// Created by @balocodes
const redis = __importStar(require("redis"));
class RedisCtrl {
    constructor() {
        this.activeClient = this.makeClient();
    }
    makeClient() {
        return redis.createClient();
    }
    watchConnection() {
        this.activeClient.on("connect", function () {
            console.log("Redis client connected");
        });
        this.activeClient.on('error', function (err) {
            console.log('Something went wrong ' + err);
        });
    }
    set(data) {
        this.activeClient.set(data.key, data.value, redis.print);
    }
    get(data) {
        this.activeClient.get(data.key, data.func);
    }
}
exports.redisCtrl = new RedisCtrl();
//# sourceMappingURL=redisCtrl.js.map