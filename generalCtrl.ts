// Created by @balocodes
import { Model, Document, Types } from "mongoose";
import { GeneralUtility } from "./genUtitlity";

interface Population {
  run_population?: boolean; // determines whether or not to run population on model
  population_path?: string; // paths to populate
  population_fields?: string; // fields of populated data to select
}

class GeneralCtrl extends GeneralUtility {
  model: Model<Document>;
  response: any;
  request: any;
  sendResp: any;
  /**
   * Expects a db model. Attempts to send a response after operation
   * except sendResp is set to false
   * @param model
   * @param sendResp
   */
  constructor(model: Model<Document>, sendResp = true) {
    super(model);
    this.model = model;
    this.sendResp = sendResp;
    // console.log(process.env.NODE_ENV);
  }

  // Generates shell from req.query
  shellGenerator(req: any) {
    let shell = this.dataCleaner(req.query);
    shell.data = this.baloPatcher(req, shell);
    if (req.params.baloPatch) {
      shell.data = { ...shell.data, ...req.params.baloPatch };
    }
    return this.dataCleaner(shell);
  }

  // Generates shell from req.body
  shellGenerator2(req: any) {
    let shell = this.dataCleaner(req.body);
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }
    if (req.params.baloPatch) {
      shell.data = { ...shell.data, ...req.params.baloPatch };
    }
    return this.dataCleaner(shell);
  }

  // Patches extra fields onto shell.data when required
  baloPatcher = (req: any, shell: any) => {
    if (req.baloPatch) {
      return { ...shell.data, ...req.baloPatch };
    } else {
      return shell.data;
    }
  };

  // Returns full err when in development mode. Used when returning response to user
  fullError(err: any) {
    if (process.env.NODE_ENV === "development") {
      return err;
    } else {
      return {};
    }
  }

  /**
   * Adds data to the db
   * ***POST***
   * @param req
   * @param res
   */
  addData(req: any, res: any, callback = this.afterRequestCallback) {
    this.response = res;
    this.request = req;
    const shell = this.dataCleaner(req.body);
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }
    return new this.model(shell.data).save((err: any, data: any) => {
      if (this.request.logger) {
        GeneralCtrl.loggerV2({
          who: req.decoded.username,
          staff: req.decoded._id,
          action: `Attempted to add data: ${
            data ? data._id : "Seemed to fail"
          }`,
          modelName: this.model.modelName,
        });
      }
      this.defaultCallback(err, data, this.respCode(err, 201, callback));
    });
  }

  /**
   * Adds data to the db
   * ***POST***
   * @param req
   * @param res
   * @param options
   */
  async addDataV2(
    req: any,
    res: any,
    options = {
      successMessage: "Data added successfully!",
      errorMessage: "Failed to add data",
    }
  ) {
    const shell = this.shellGenerator2(req);
    return await new this.model(shell.data)
      .save()
      .then((data) => {
        if (req.logger) {
          GeneralCtrl.logger(
            req.decoded._id,
            `Attempted to add data: ${data._id}`,
            this.model.modelName
          );
        }
        // this.successHandler();
        GeneralUtility.successHandler({
          io: req.IO,
          event: req.params.IOEvent,
          content: req.params.IOContent,
        });
        return { result: data, message: options.successMessage, error: false };
      })
      .catch((err) => {
        GeneralCtrl.printData("Error while adding data", err);
        if (req.logger) {
          GeneralCtrl.loggerV2({
            who: req.decoded.username,
            staff: req.decoded._id,
            action: `Attempted to add data: "Seemed to fail"}`,
            modelName: this.model.modelName,
          });
        }
        this.errorHandler();
        return {
          message: options.errorMessage,
          error: true,
          fullError: this.fullError(err),
        };
      });
  }

  /**
   * Add Many Items at once. Expects an array in the
   * form of req.body.data
   * @param req Request
   * @param res Response
   */
  addMany(req: any, res: any) {
    this.response = res;
    this.request = req;
    this.model.insertMany(
      req.body.data,
      {
        ordered: false,
      },
      (err: any, data: any) => {
        if (this.request.logger) {
          GeneralCtrl.logger(
            this.request.decoded._id,
            "Attempted to add multiple data",
            this.model.modelName
          );
        }
        if (err) {
          GeneralCtrl.printData(err);
          this.response.send({
            message: "An error occured. Ignoring duplicate data",
            error: true,
          });
          if (this.request.logger) {
            GeneralCtrl.logger(
              this.request.decoded._id,
              "Failed: Add many failed",
              this.model.modelName
            );
          }
        } else {
          if (this.request.logger) {
            GeneralCtrl.logger(
              this.request.decoded._id,
              "Added multiple data successfully!",
              this.model.modelName
            );
          }
          this.response.send({
            message: `Data successfully added ${data.length} entries`,
            error: false,
          });
        }
      }
    );
  }

  async addManyV2(
    req: any,
    res: any,
    options = {
      successMessage: "Data added successfully!",
      errorMessage: "Failed to add data",
    }
  ) {
    return await this.model
      .insertMany(req.body.data, { ordered: false })
      .then((data) => {
        if (req.logger) {
          GeneralCtrl.logger(
            req.decoded._id,
            `Attempted to add multiple data: "Successfully added data"}`,
            this.model.modelName
          );
        }
        this.successHandler();
        return { result: data, message: options.successMessage, error: false };
      })
      .catch((err) => {
        if (req.logger) {
          GeneralCtrl.loggerV2({
            who: req.decoded.username,
            staff: req.decoded._id,
            action: `Attempted to add multiple data:  "Seemed to fail"}`,
            modelName: this.model.modelName,
          });
        }
        this.errorHandler();
        return {
          message: options.errorMessage,
          error: true,
          fullError: this.fullError(err),
        };
      });
  }

  async addManyWithoutRequest(data: any[]) {
    return await this.model
      .insertMany(data, { ordered: false })
      .then((data) => {
        if (data && data.length > 0) {
          return {
            result: data,
            message: "Operation Successful",
            error: false,
          };
        } else {
          return { message: "No data was added", error: false };
        }
      })
      .catch((e) => {
        return { message: "An error occured", error: true, fullError: e };
      });
  }

  /**
   * Fetch data from db
   * ***GET***
   * @param req
   * @param res
   */
  readData(
    req: any,
    res: any,
    callbackRunner = {
      population: false,
      population_config: {
        pathOfModelToPopulate: "",
        fields: "",
      },
    }
  ) {
    this.response = res;
    this.request = req;
    let shell = this.dataCleaner(req.query);
    // Balo patch extends the content of the data after it has been cleaned
    // Makes it easier to patch extra parameters to query.
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }
    this.model
      .find(shell.data)
      .limit(shell.limit)
      .skip(shell.page * shell.limit)
      .sort(shell.order)
      .select(shell.fields)
      .exec((err: any, data: any) => {
        if (callbackRunner.population) {
          this.populateModel(data, callbackRunner.population_config);
        } else {
          this.defaultCallback(err, data, this.respCode(err, 200));
        }
      });
  }

  async readDataV2(
    req: any,
    res?: any,
    options = {
      successMessage: "Operation Successful!",
      errorMessage: "Failed to read data",
    },
    population: Population = { run_population: false }
  ) {
    const shell = this.shellGenerator(req);

    if (req.params.run_population) {
      population.run_population = true;
      population.population_fields = req.params.population_fields;
      population.population_path = req.params.population_path;
    }

    return population.run_population
      ? await this.model
          .find(shell.data)
          .limit(shell.limit)
          .skip(shell.limit * shell.page)
          .sort(shell.order)
          .select(shell.fields)
          .populate(population.population_path, population.population_fields)
          .then((data) => {
            return {
              result: data,
              message: options.successMessage,
              error: false,
            };
          })
          .catch((err) => {
            console.log("err 1", err);
            return {
              message: options.errorMessage,
              error: true,
              fullError: this.fullError(err),
            };
          })
      : await this.model
          .find(shell.data)
          .limit(shell.limit)
          .skip(shell.limit * shell.page)
          .sort(shell.order)
          .then((data) => {
            return {
              result: data,
              message: options.successMessage,
              error: false,
            };
          })
          .catch((err) => {
            console.log(err);
            return {
              message: options.errorMessage,
              error: true,
              fullError: this.fullError(err),
            };
          });
  }

  /**
   * Fetch data by ID
   * ***GET***
   * @param req
   * @param res
   */
  readDataById(
    req: any,
    res: any,
    callbackRunner = {
      population: false,
      population_config: {
        pathOfModelToPopulate: "",
        fields: "",
      },
    }
  ) {
    this.response = res;
    this.request = req;
    if (!req.data.query._id) {
      return this.defaultCallback(
        { code: "NO_ID", message: "No ID present on data" },
        null
      );
    }
    this.model.findById(req.data.query._id).exec((err: any, data: any) => {
      if (callbackRunner.population) {
        this.populateModel(data, callbackRunner.population_config);
      } else {
        this.defaultCallback(err, data, this.respCode(err, 200));
      }
    });
  }

  async readDataByIdV2(
    req: any,
    res?: any,
    options = {
      successMessage: "Operation Successful!",
      errorMessage: "Failed to read data",
    },
    population: Population = { run_population: false }
  ) {
    const shell = this.shellGenerator(req);

    if (req.params.run_population) {
      population.run_population = true;
      population.population_fields = req.params.population_fields;
      population.population_path = req.params.population_path;
    }

    return population.run_population
      ? await this.model
          .find(shell.data)
          .limit(shell.limit)
          .skip(shell.limit * shell.page)
          .sort(shell.order)
          .select(shell.fields)
          .populate(population.population_path, population.population_fields)
          .then((data) => {
            return {
              result: data,
              message: options.successMessage,
              error: false,
            };
          })
          .catch((err) => {
            return {
              message: options.errorMessage,
              error: true,
              fullError: this.fullError(err),
            };
          })
      : await this.model
          .find(shell.data)
          .limit(shell.limit)
          .skip(shell.limit * shell.page)
          .select(shell.fields)
          .sort(shell.order)
          .then((data) => {
            return {
              result: data,
              message: options.successMessage,
              error: false,
            };
          })
          .catch((err) => {
            return {
              message: options.errorMessage,
              error: true,
              fullError: this.fullError(err),
            };
          });
  }

  /**
   * Fetch single item from db
   * ***GET***
   * @param req
   * @param res
   */
  readSingleItem({
    req,
    res,
    callbackRunner = {
      population: false,
      population_config: {
        pathOfModelToPopulate: "",
        fields: "",
      },
    },
  }: {
    req: any;
    res: any;
    callbackRunner?: {
      population: boolean;
      population_config: { pathOfModelToPopulate: string; fields: string };
    };
  }) {
    this.response = res;
    this.request = req;
    const shell = this.dataCleaner(req.query);
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }

    this.model
      .findOne(shell.data)
      .select(shell.fields)
      .exec((err: any, data: any) => {
        if (callbackRunner.population) {
          this.populateModel(data, callbackRunner.population_config);
        } else {
          this.defaultCallback(err, data, this.respCode(err, 200));
        }
      });
  }

  async readSingleItemV2({
    req,
    _res,
    options = {
      successMessage: "Operation Successful!",
      errorMessage: "Operation failed!",
    },
    population = { run_population: false },
  }: {
    req: any;
    _res?: any;
    options?: { successMessage: string; errorMessage: string };
    population?: Population;
  }) {
    const shell = this.shellGenerator(req);

    if (req.params.run_population) {
      population.run_population = true;
      population.population_fields = req.params.population_fields;
      population.population_path = req.params.population_path;
    }

    return population.run_population
      ? await this.model
          .findOne(shell.data)
          .skip(shell.limit * shell.page)
          .sort(shell.order)
          .select(shell.fields)
          .populate(population.population_path, population.population_fields)
          .then((data: Document | null) => {
            return {
              result: data,
              message: options.successMessage,
              error: false,
            };
          })
          .catch((err) => {
            return {
              result: null,
              message: options.errorMessage,
              error: true,
              fullError: this.fullError(err),
            };
          })
      : await this.model
          .findOne(shell.data)
          .skip(shell.limit * shell.page)
          .select(shell.fields)
          .sort(shell.order)
          .then((data: Document | null) => {
            return {
              result: data,
              message: data ? options.successMessage : "No document found.",
              error: false,
            };
          })
          .catch((err) => {
            return {
              result: null,
              message: options.errorMessage,
              error: true,
              fullError: this.fullError(err),
            };
          });
  }

  /**
   * Find by id and update
   * ***PUT***
   * @param req
   * @param res
   */
  updateDataById(req: any, res: any, callback = this.afterRequestCallback) {
    this.response = res;
    this.request = req;
    // this.sendResp = true;
    const shell = this.dataCleaner(req.body);
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }
    if (!shell.data._id) {
      return this.defaultCallback(
        { code: "NO_ID", message: "No ID present on data" },
        null
      );
    }
    this.model
      .findByIdAndUpdate(shell.data._id, { $set: shell.data })
      .exec((err: any, data: any) => {
        if (this.request.logger) {
          GeneralCtrl.logger(
            this.request.decoded._id,
            `Attempted to update data: ${shell.data._id}`,
            this.model.modelName,
            data
          );
        }
        this.defaultCallback(err, data, this.respCode(err, 200, callback));
      });
  }

  async updateDataByIdV2(
    req: any,
    res: any,
    options = {
      successMessage: "Data updated successfully!",
      errorMessage: "Failed to update data",
    }
  ) {
    const shell = this.shellGenerator2(req);

    return await this.model
      .findByIdAndUpdate(shell.data._id, { $set: shell.data })
      .then((data: Document | null) => {
        if (req.logger) {
          GeneralCtrl.logger(
            req.decoded._id,
            `Attempted to update data: ${data ? data._id : ""}`,
            this.model.modelName,
            data
          );
        }
        this.successHandler();
        return {
          result: data,
          message: data ? options.successMessage : "Document not found.",
          error: false,
        };
      })
      .catch((err) => {
        if (req.logger) {
          GeneralCtrl.loggerV2({
            who: req.decoded.username,
            staff: req.decoded._id,
            action: `Attempted to update data: ${shell.data._id} but "Seemed to fail"}`,
            modelName: this.model.modelName,
          });
        }
        this.errorHandler();
        return {
          message: options.errorMessage,
          error: true,
          fullError: this.fullError(err),
        };
      });
  }

  /**
   * Find and update. Requires a find field in the request body
   * ***PUT***
   * @param req
   * @param res
   */
  updateData(req: any, res: any, callback = this.afterRequestCallback) {
    this.response = res;
    this.request = req;
    const shell = this.dataCleaner(req.body);
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }
    this.model.update(shell.find, shell.data).exec((err: any, data: any) => {
      if (this.request.logger) {
        GeneralCtrl.logger(
          this.request.decoded._id,
          `Attempted to update data: ${JSON.stringify(shell.data.find)}`,
          this.model.modelName,
          data
        );
      }
      this.defaultCallback(err, data, this.respCode(err, 200, callback));
    });
  }

  /**
   * Find document by id and delete
   * ***DELETE***
   * @param req
   * @param res
   */
  deleteDataById(req: any, res: any, callback = this.afterRequestCallback) {
    this.response = res;
    this.request = req;
    const shell = this.dataCleaner(req.body);
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }
    if (!shell.data._id) {
      return this.defaultCallback(
        { code: "NO_ID", message: "No ID present on data" },
        null
      );
    }
    this.model.findByIdAndDelete(shell.data._id).exec((err: any, data: any) => {
      if (req.logger) {
        GeneralCtrl.logger(
          this.request.decoded._id,
          `Attempted to delete data: ${shell.data._id}`,
          this.model.modelName,
          data
        );
      }
      this.defaultCallback(err, data, this.respCode(err, 200, callback));
    });
  }

  async deleteDataByIdV2(
    req: any,
    res: any,
    options = {
      successMessage: "Data deleted successfully!",
      errorMessage: "Failed to update data",
    }
  ) {
    const shell = this.shellGenerator2(req);
    return await this.model
      .findByIdAndDelete(shell.data._id)
      .then((data: Document | null) => {
        if (req.logger) {
          GeneralCtrl.logger(
            req.decoded._id,
            data
              ? `Attempted to delete data: ${shell.data._id}`
              : "Attempted to delete noexistent data",
            this.model.modelName,
            data
          );
        }
        GeneralCtrl.printData("delete data", data);
        this.successHandler();
        return {
          result: data,
          message: data
            ? options.successMessage
            : "Did not find document to delete.",
          error: false,
        };
      })
      .catch((err) => {
        if (req.logger) {
          GeneralCtrl.logger(
            req.decoded._id,
            `Attempted to delete data: ${shell.data._id} but failed!`,
            this.model.modelName
          );
        }
        console.log("delete error", err);
        this.errorHandler();
        return {
          message: options.errorMessage,
          error: true,
          fullError: this.fullError(err),
        };
      });
  }

  async searchDataV2({
    req,
    _res,
    options = {
      successMessage: "Operation Successful!",
      errorMessage: "Operation failed!",
    },
    population = { run_population: false },
  }: {
    req: any;
    _res?: any;
    options?: { successMessage: string; errorMessage: string };
    population?: Population;
  }) {
    const shell = this.shellGenerator(req);
    if (req.params.run_population) {
      population.run_population = true;
      population.population_fields = req.params.population_fields;
      population.population_path = req.params.population_path;
    }
    if (population.run_population) {
      return await this.model
        .find(
          { $text: { $search: shell.data.search, $diacraticSensitive: false } },
          { score: { $meta: "textScore" } }
        )
        .populate(population.population_path, population.population_fields)
        .limit(shell.limit)
        .skip(shell.page * shell.limit)
        .select(shell.fields)
        .sort(shell.order)
        .then((data) => {
          return { result: data, message: null, error: false };
        })
        .catch((err) => {
          console.log(err);
          return { result: null, message: "An error occured", error: true };
        });
    } else {
      return await this.model
        .find(
          { $text: { $search: shell.data.search, $diacraticSensitive: false } },
          { score: { $meta: "textScore" } }
        )
        .select(shell.fields)
        .then((data) => {
          return { result: data, message: null, error: false };
        })
        .catch((err) => {
          console.log(err);
          return { result: null, message: "An error occured", error: true };
        });
    }
  }

  /**
   * Search for data in db. Requires a search field.
   * ***GET***
   * @param req
   * @param res
   */
  searchData(req: any, res: any) {
    this.response = res;
    this.request = req;
    const shell = this.dataCleaner(req.query);
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }
    this.model
      .find(
        { $text: { $search: shell.data.search, $diacraticSensitive: false } },
        { score: { $meta: "textScore" } }
      )
      .limit(shell.limit)
      .skip(shell.page * shell.limit)
      .sort({ score: { $meta: "textScore" } })
      .select(shell.fields)
      .exec((err: any, data: any) => {
        this.defaultCallback(err, data, this.respCode(err, 200));
      });
  }

  /**
   * Populate nested models in result by id
   * @param results
   * @param obj
   */
  populateModel(results: any, obj: any) {
    this.model.populate(
      results,
      {
        path: obj.pathOfModelToPopulate,
        select: obj.fields,
      },
      (err: any, data: any) => {
        if (err) {
          GeneralCtrl.printData(err);
          if (this.response != null) {
            this.response.status(500).send({
              message: "An error occured during population",
              code: 500,
              error: true,
            });
          }
        } else if (this.response != null) {
          this.response.status(200).send({
            result: data,
            message: "Query Successful",
            code: 200,
            error: false,
          });
        }
      }
    );
  }

  /**
   * Returns the estimated count of documents in a collection using
   * the meta data. May not be as accurate as a full count.
   * **GET**
   * @param req
   * @param res
   */
  estimatedCount(req: any, res: any) {
    this.request = req;
    this.response = res;
    this.model.estimatedDocumentCount((err, count) => {
      this.defaultCallback(err, { count: count }, this.respCode(err, 200));
    });
  }

  async estimatedCountV2(req: any, res: any) {
    return await this.model
      .estimatedDocumentCount()
      .then((count) => {
        return { count: count, error: false, message: "Operation Successful" };
      })
      .catch((err) => {
        return {
          error: true,
          message: "Failed to get estimated number of items",
          fullError: this.fullError(err),
        };
      });
  }

  /**
   * Increase or decrease value of a field
   * @param data e.g. { find: {_id: "57485"}, inc: {age: 2} }
   */
  async incrementor(
    model: Model<Document>,
    data: { find: { [params: string]: any }; inc: { [params: string]: any } }
  ) {
    return await model.update(data.find, { $inc: data.inc });
  }

  /**
   * Returns the estimated count of documents in a collection using
   * the meta data. May not be as accurate as a full count.
   * **GET**
   * @param req
   * @param res
   */
  countData(req: any, res: any) {
    this.response = res;
    this.request = req;
    const shell = this.dataCleaner(req.body);
    if (req.baloPatch) {
      shell.data = { ...shell.data, ...req.baloPatch };
    }
    this.model.count(shell.data, (err, count) => {
      this.defaultCallback(err, { count: count }, this.respCode(err, 200));
    });
  }

  async countDataV2(req: any, res: any) {
    const shell = this.shellGenerator(req);
    return await this.model
      .count(shell.data)
      .then((count) => {
        return { count: count, error: false, message: "Operation Successful" };
      })
      .catch((err) => {
        return {
          error: true,
          message: "Failed to count documents",
          fullError: this.fullError(err),
        };
      });
  }

  /**
   * Returns false if data exists and true if it does not
   * Expects a query object to search
   * @param query
   */
  async isDataUnique(query: any) {
    const data: any = await this.model.findOne(query);
    if (data) {
      return false;
    } else {
      return true;
    }
  }
}

export const GenCtrl = GeneralCtrl;
