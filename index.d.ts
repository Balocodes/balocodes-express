declare module "express" {
    interface Request {
        params: any;
        baloPatch?: object;
        logger?: boolean;
    }
}
export * from "./generalCtrl";
export * from "./genUtitlity";
export * from "./mailgunCtrl";
export * from "./candies/consumable";
export * from "./misc";
export * from "./models/log/errorLog";
export * from "./models/log/log";
export * from "./transactionChainer";
