// Created by @balocodes
import { logModel } from "./models/log/log";
import { errorLogModel } from "./models/log/errorLog";
import { Model, Document } from "mongoose";
import { dataCleaner, filterObject } from "./misc";
import { Socket } from "socket.io";

export class GeneralUtility {
  static io: Socket;
  model: Model<Document>;
  response: any;
  request: any;
  sendResp: any;
  dataCleaner = dataCleaner;
  afterRequestCompletedHasError = false;
  io: any;
  ioEvent: any;
  ioContent = {};
  defaultMessages = {
    successMessage: "Operation successful",
    errorMessage: "OperationFailed"
  };

  constructor(
    model: Model<Document>,
    response?: any,
    request?: any,
    sendResp?: any
  ) {
    this.model = model;
    this.response = response;
    this.request = request;
    this.sendResp = sendResp;
  }

  static logger(
    who: any,
    action: string,
    model_name = "",
    previous_data: Document | null = null
  ) {
    logModel.create([
      {
        who: who,
        action: action,
        model_name: model_name,
        previous_data: previous_data
      }
    ]);
  }

  static loggerV2({
    who = "",
    staff,
    action = "",
    modelName = "",
    previousData = null
  }: {
    who: string;
    staff?: string;
    action: string;
    modelName: string;
    previousData?: Document | null;
  }) {
    logModel.create([
      {
        who: who,
        staff: staff,
        action: action,
        model_name: modelName,
        previous_data: previousData
      }
    ]);
  }

  static errorLogger(error: string) {
    errorLogModel.create([{ error: error }]);
  }

  defaultOptions() {
    return {
      sendResp: true
    };
  }

  /**
   * Works exactly like console.log
   * The only difference is that it only logs data when NODE_ENV=development
   * Otherwise, it does nothing.
   * @param param1
   * @param param2
   */
  static printData(...params: any) {
    if (process.env.NODE_ENV === "development") {
      console.log(params);
    } else {
      console.log("NODE_ENV", "Cannot Log Data in production mode");
    }
  }

  /**
   * Decides whether or not response is sent to client and what format
   * @param err
   * @param data
   * @param code
   */
  async defaultCallback(err: any, data: any, code = 200) {
    let errStatusCode = 500;
    if (err) {
      err.code === null
        ? GeneralUtility.printData("Error", err)
        : GeneralUtility.printData("Error", err.code);
      switch (err.code) {
        case 11000:
          GeneralUtility.printData("Error", err);
          errStatusCode = 409;
          err.message = "Data already exists!";
          if (this.request.logger) {
            GeneralUtility.logger(
              this.request.decoded._id,
              `Duplicate error: ${err.code}`,
              this.model.modelName
            );
          }
          break;

        case "NO_ID":
          GeneralUtility.printData(err);
          errStatusCode = 404;
          err.message = "Could not identify data.";
          if (this.request.logger) {
            GeneralUtility.logger(
              this.request.decoded._id,
              `Tried to update without ID: ${err.code}`,
              this.model.modelName
            );
          }
          break;

        default:
          err.message = "Operation failed!";
          if (this.request.logger) {
            GeneralUtility.logger(
              this.request.decoded._id,
              `Unknown error: ${err.code}`,
              this.model.modelName
            );
          }
          break;
      }
      if (this.sendResp) {
        GeneralUtility.printData("Error", JSON.stringify(err));
        this.response.status(errStatusCode).send({
          error: true,
          message: err.message,
          code: err.code
        });
        this.response.end();
      }
      if (this.request.logger) {
        GeneralUtility.logger(
          this.request.decoded._id,
          `Operation failed: ${err.code}`,
          this.model.modelName
        );
      }
    } else if (data) {
      if (this.sendResp) {
        this.response.status(code).send({
          error: false,
          result: await filterObject(data, data.fieldsToRemove),
          message: "Operation Successful!"
        });
        this.response.end();
      }
      if (this.request.logger) {
        GeneralUtility.logger(
          this.request.decoded._id,
          "Updated/Added data successfully!",
          this.model.modelName
        );
      }
    } else {
      if (this.sendResp) {
        this.response.status(code).send({
          error: false,
          result: data,
          message: "Operation did not throw any error but no response either"
        });
        this.response.end();
      }
      if (this.request.logger) {
        GeneralUtility.logger(
          this.request.decoded._id,
          "Generic Response",
          this.model.modelName
        );
      }
    }
  }

  errorHandler() {
    //pass
  }
  successHandler() {
    //pass
    if (this.io) {
      this.io.emit(this.ioEvent, this.ioContent);
      this.ioEvent = null;
    }
  }

  static successHandler({
    io,
    event,
    content = ""
  }: {
    io: Socket;
    event: string;
    content: any;
  }) {
    if (event) {
      io.emit(event, content);
    }
  }

  /**
   * A callback ran after request is completed.
   * @param errFunc
   * @param successFunc
   * @param isError
   */
  afterRequestCallback = (hasError: boolean) => {
    if (hasError) {
      this.errorHandler();
    } else {
      this.successHandler();
    }
  };

  /**
   * Returns response code
   * @param err
   * @param code
   */
  respCode(err = null, code = 200, callback = this.afterRequestCallback) {
    if (err) {
      this.afterRequestCompletedHasError = true;
      callback(true);
      return 500;
    }
    this.afterRequestCompletedHasError = false;
    callback(false);
    return code;
  }
}
